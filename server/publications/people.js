Meteor.publish('people', function () {
	// If we had users, then we'd specify criteria for accessing people here
	return db.people.find();
});