Meteor.publish('meetings', function () {
	// If we had user management, then we'd specify criteria for accessing meetings
	// information here
	return db.meetings.find();
});