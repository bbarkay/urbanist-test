Meteor.methods({
	'addMeeting' : function(title, date) {
		check(title, String);
		check(date, Date);
		if (title.length === 0) {
			throw new Meteor.Error('empty-title', 'Meeting titles cannot be empty');
		}
		return db.meetings.insert({ 'title' : title, 'date' : date, 'guests' : []});
	},
	'removeMeeting' : function(meetingId) {
		check(meetingId, String);
		return db.meetings.remove({_id : meetingId});
	},
	'addPerson' : function(name, pictureUrl) {
		check(name, String);
		check(pictureUrl, String);
		return db.people.insert({ 'name' : name, 'pictureUrl' : pictureUrl });
	},
	'updateGuestStatus' : function(meetingId, personId, status) {
		check(meetingId, String);
		check(personId, String);
		check(status, String);
		var meeting = db.meetings.findOne({ '_id' : meetingId }), i, guest;
		for (i = 0; i < meeting.guests.length; i++) {
			guest = meeting.guests[i];
			if (guest.personId === personId) {
				guest.status = status;
				break;
			}
		}
		if (i === meeting.guests.length) {
			meeting.guests.push({ 'personId' : personId, 'status' : status });
		}
		return db.meetings.update(meetingId, meeting);
	},

	'removeGuest' : function (meetingId, personId) {
		check(meetingId, String);
		check(personId, String);
		console.log('remove guest',meetingId,personId);
		var meeting = db.meetings.findOne({ '_id' : meetingId }), i;
		for (i = 0; i < meeting.guests.length; i++) {
			if (meeting.guests[i].personId === personId) {
				meeting.guests.splice(i,1);
				break;
			}
		}
		return db.meetings.update(meetingId, meeting);
	},

	'truncatePeople' : function() {
		return db.people.remove({});
	}
});