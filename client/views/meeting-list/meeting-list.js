Template.meetingList.helpers({
	'meetings' : function () {
		return db.meetings.find({}, { fields: { _id: 1, title : 1, date : 1 } });
	}
});

Template.meetingListItem.helpers({
	'selectedClass' : function () {
		return Session.get('meetingId') === this._id ? 'selected' : '';
	}
});

Template.meetingListItem.events({
	'click .meeting' : function(event) {
		app.selectMeeting(this._id);
	}
});