var groupList = {
	// would probably want localization for the title
	'attending' : { title : 'Attending' },
	'maybe' : { title : 'Maybe' },
	'declined' : { title : 'Declined' }
};

Template.meetingDetails.helpers({
	'meeting' : function () {
		var objectId = Session.get('meetingId');
		if (!objectId) {
			return false;
		}
		return db.meetings.findOne({_id:objectId});
	}
});

Template.guests.helpers({
	'groups' : function () {
		var groups = Object.create(groupList), groupArray = [], key;
		for(key in groups) {
			groups[key].people = [];
		}

		this.guests.forEach(function(guest) {
			if (groups[guest.status]) {
				groups[guest.status].people.push(guest.personId);
			} else {
				throw 'Unknown guest status: "' + guest.status + '"';
			}
		});

		for (key in groups) {
			groupArray[groupArray.length] = groups[key];
		}
		return groupArray;
	}
})

Template.availablePeople.helpers({
	'available' : function () {
		var key, existingIds = [];

		this.guests.forEach(function (guest) {
			existingIds.push(guest.personId);
		});

		return db.people.find({_id : {$nin : existingIds}}).fetch();
	}
});

Template.person.helpers({
	'person' : function() {
		return db.people.findOne({_id:this.id});
	},

	'selectedClass' : function() {
		return Session.get('personId') === this.id ? 'selected' : '';
	}
});

Template.person.events({
	'click .person' : function () {
		Session.set('personId', this.id);
	}
});

Template.editAttendanceForm.helpers({
	'meetingAndPersonSelected' : function () {
		return Session.get('meetingId') && Session.get('personId');
	},
	'personName' : function () {
		return db.people.findOne({_id : Session.get('personId')}).name;
	},
	'options' : function () {
		var options = $.extend({}, groupList),
			personId = Session.get('personId'),
			meeting = db.meetings.findOne({ '_id' : Session.get('meetingId') }),
			i;

		for (i = 0; i < meeting.guests.length; i++) {
			if (meeting.guests[i].personId === personId) {
				delete options[meeting.guests[i].status];
				break;
			}
		}
		if (i !== meeting.guests.length) {
			// person one of guests groups
			options['uninvited'] = {'title':'Not invited'};
		}

		// looks like I have an outdated version, so I can't ite

		return options;
	}
});

Template.editAttendanceForm.events({
	'click .cancel-button' : function (event) {
		event.preventDefault();
		Session.set('personId', undefined);
	},
	'submit form' : function (event) {
		event.preventDefault();
		var meetingId = Session.get('meetingId'),
			personId = Session.get('personId'),
			status = event.target.status.value;
		if (status === 'uninvited') {
			app.removeGuest(meetingId, personId);
		} else {
			app.updateGuestStatus(meetingId, personId, status);
		}
		Session.set('personId', false);
	}
});

Template.removeMeetingForm.events({
	'submit form' : function (event) {
		event.preventDefault();
		app.removeMeeting(this._id);
	}
})