Template.addMeetingForm.events({
	'submit form' : function (event) {
		event.preventDefault();
		var form = event.target;
		app.addMeeting(form.title.value, form.date.value);
		form.reset();
	}
});