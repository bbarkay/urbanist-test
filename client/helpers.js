// Formats dates with moment
Template.registerHelper('formatDate', function (date, format) {
	if (typeof(format) !== 'string') {
		format = 'DD MMM, YYYY @ HH:MM';
	}
	return moment(date).format(format);
});

Template.registerHelper('toArray', function (object, sort) {
	var array = [];
	for (var key in object) {
		array.push({key:key, value:object[key]});
	}
	return array;
});