Meteor.subscribe('meetings');
Meteor.subscribe('people');

app = {
	addMeeting : function (title, date) {
		// validation should be done in client-side as well, and show specific messages and mark the
		// invalid input fields. I neglected this for this test
		console.log('Adding meeting:', title, date);
		if (typeof(date) === 'string') {
			date = new Date(date);
		}
		Meteor.call('addMeeting', title, date, function (error, meetingId) {
			if (error) {
				console.log(error);
				alert('Ensure that both title and date are correct');
			} else {
				app.selectMeeting(meetingId);
			}
		});
	},

	selectMeeting : function(meetingId) {
		console.log('Selecting meeting', meetingId);
		Session.set('meetingId', meetingId);
	},

	removeMeeting : function(meetingId) {
		Meteor.call('removeMeeting', meetingId);
	},

	updateGuestStatus : function(meetingId, personId, status) {
		Meteor.call('updateGuestStatus', meetingId, personId, status);
	},

	removeGuest : function(meetingId, personId) {
		Meteor.call('removeGuest', meetingId, personId);
	}
};